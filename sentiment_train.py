import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from tensorflow.keras.layers import (
    Dense,
    Dropout,
    Embedding,
    LSTM,
    SpatialDropout1D,
    Bidirectional,
    BatchNormalization,
)
from tensorflow.keras.models import load_model, Sequential
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from gensim.models import KeyedVectors


class SentimentProcess(object):
    def __init__(self):
        self.w2v_model = self._open_w2v_model()

        # Model parameters
        self.sequence_len = 500
        self.embeddings_dim = 100
        self.batch_size = 32
        self.num_epochs = 30
        self.model_path = "./models/nn_model.h5"
        self.num_classes = 5
        self.target_dict = {
            "Негативная": 0,
            "Положительная": 1,
            "Нейтрально (-)": 2,
            "Нейтрально (+)": 3,
            "Нейтральная": 4,
        }

    def _open_w2v_model(self):
        model = KeyedVectors.load_word2vec_format(
            "./models/w2vmodel_good.bin",
            binary=True,
            encoding="utf-8",
            unicode_errors="ignore",
        )
        print("Len of the model vocab: ", len(model.vocab))
        pretrained_weights = model.syn0
        self.embedding_matrix = pretrained_weights
        self.vocab_size, self.embeddings_dim = self.embedding_matrix.shape
        print(self.embedding_matrix)
        print(self.vocab_size, self.embeddings_dim)
        print("Word2vec model is loaded")
        return model

    def word2idx(self, word):
        try:
            return self.w2v_model.vocab[word].index
        except KeyError:
            return 99999

    def idx2word(self, idx):
        return self.w2v_model.index2word[idx]

    def get_word_grammar_string(self, text):
        """
        Parse text + title, takes lemma + grammar
        and convert to array of vectors for text"""
        # call for parser (text)
        text_words = []
        for paragraph in parse_result:
            for sentence in paragraph:
                for token in sentence:
                    text, lemma, grammar = token
                    result_token = lemma.lower() + "_" + grammar
                    text_words.append(result_token)
        return text_words

    def make_model(self, X_train, X_test, y_train, y_test):
        """ NN model """
        print(self.vocab_size, self.embeddings_dim, X_train.shape[1])

        model = Sequential()
        model.add(
            Embedding(
                self.vocab_size, self.embeddings_dim, weights=[self.embedding_matrix]
            )
        )
        model.add(SpatialDropout1D(0.6))
        model.add(Bidirectional(LSTM(128)))
        model.add(Dropout(0.6))
        model.add(BatchNormalization())
        model.add(Dense(5, activation="softmax"))

        model.compile(
            optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"]
        )
        print(model.summary())

        checkpoint = ModelCheckpoint(
            self.model_path,
            save_best_only=True,
            monitor="val_acc",
            verbose=1,
            mode="max",
        )
        callbacks_list = [checkpoint]

        model.fit(
            X_train,
            y_train,
            epochs=self.num_epochs,
            batch_size=self.batch_size,
            validation_split=0.3,  # validation split
            verbose=1,
            callbacks=callbacks_list,
        )

        accr = model.evaluate(X_test, y_test)
        print("Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}".format(accr[0], accr[1]))

    def prepare_dataset(self, clean_data):
        """ Convert dataset to good format"""
        print("Start preparing dataset")
        X_data = np.zeros([len(clean_data), self.sequence_len], dtype=np.int32)
        y_data = np.zeros([len(clean_data)], dtype=np.int32)

        max_number_of_words = []
        # scaler = StandardScaler()
        for index, row in clean_data.iterrows():
            my_text = str(row["Title"]) + str(row["Text"])
            text_words = self.get_word_grammar_string(my_text)
            max_number_of_words.append(len(text_words))
            for t, word in enumerate(text_words):
                vector_index = self.word2idx(word)
                if vector_index != 99999:
                    try:
                        X_data[index, t] = vector_index
                    # if the text is more than SeqLen tokens \
                    # just cut it, takes only seq_len set
                    except IndexError:
                        continue
            y_data[index] = self.target_dict[row["Sentiment"]]

        print("Initial datasets: ")
        print(X_data.shape)
        print(y_data)
        print("Max number of words in text: ", max(max_number_of_words))

        y_data = np_utils.to_categorical(y_data)
        print(y_data)

        print("Initial shape of train data", X_data.shape)

        X_data_smooth = pad_sequences(X_data, maxlen=self.sequence_len)

        print("After padding X:")
        print(X_data_smooth.shape)

        print("Targets:")
        print(y_data.shape)

        return X_data_smooth, y_data


def make_dataset_and_train(filename):
    """Makes dataset from data"""
    row_data = pd.read_csv(filename, sep="\t")

    clean_data = row_data[pd.notnull(row_data["Sentiment"])]

    process_obj = SentimentProcess()

    X_data, y_data = process_obj.prepare_dataset(clean_data)

    X_train, X_test, y_train, y_test = train_test_split(
        X_data, y_data, test_size=0.2, random_state=40
    )

    print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

    print("Start training")
    process_obj.make_model(X_train, X_test, y_train, y_test)


def make_dataset_and_test(filename):
    """ """
    row_data = pd.read_csv(filename, sep="\t")

    clean_data = row_data[pd.notnull(row_data["Sentiment"])]

    process_obj = SentimentProcess()
    # X_data_test, y_data_test = process_obj.prepare_dataset(clean_data)
    X_data_test, y_data_test = process_obj.prepare_dataset(clean_data.head(2000))

    my_network_model = load_model(process_obj.model_path)
    classes = my_network_model.predict_classes(X_data_test)
    print(classes)
    score = my_network_model.evaluate(X_data_test, y_data_test)
    print("My score: ", score)
    return score


if __name__ == "__main__":

    """ Filename with fields 'Sentiment', 'Title', 'Text' """
    make_dataset_and_train("sentiment_data.csv")
    make_dataset_and_test("sentiment_test.csv")
