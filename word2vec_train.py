import gensim
from gensim.models import KeyedVectors
from pymongo import MongoClient
from gensim.models.fasttext import FastText
from gensim.models.wrappers.fasttext import FastText as FT_wrapper

from gensim.models.word2vec import LineSentence


def the_same_as_mysentence():
    lee_data = LineSentence("./text_for_fasttext.txt")
    return lee_data


class MySentences(object):
    def __init__(self, train_type="w2v"):
        self.db_adress = "mongodb://"
        self.texts = self._load_dataset_from_mongo()
        self.train_type = train_type

    def __iter__(self):
        for my_text in self.texts:
            text_sentences = self.add_grammar(my_text)
            yield text_sentences  # [ text with spaces ]

    def _load_dataset_from_mongo(self):
        """ Load data from db
        """
        print("Go to mongo!")
        connection = MongoClient(self.db_adress)
        collection = connection["CollectionName"]
        posts = collection["Posts"]
        texts = []
        for n, some_post in enumerate(posts.find({"_Language": 10})):
            try:
                text = some_post["Text"]
                texts.append(text)
            except KeyError:
                print("No Text in post")
            try:
                title = some_post["Title"]
                texts.append(title)
            except KeyError:
                print("No Title in post")
        connection.close
        print("Close connection")
        return texts

    def add_grammar(self, text):
        """Add pos tags from Texts process lib"""
        # call for parser (text)
        text = []
        for paragraph in parse_result:
            for sentence in paragraph:
                sent = []
                for token in sentence:
                    if self.train_type == "w2v":
                        text, lemma, grammar = token
                        result_token = lemma.lower() + "_" + grammar
                        sent.append(result_token)
                    if self.train_type == "fasttext":
                        sent.append(token[1])
                text.extend(sent)
        return text


def train_word2vec_model():
    """Train model on the data with UD grammar"""
    sentences = MySentences()
    model = gensim.models.Word2Vec(sentences, min_count=5)
    model.wv.save_word2vec_format("./models/w2vmodel_good.bin", binary=True)
    print("Model is done")


def check_w2v_model():
    model = KeyedVectors.load_word2vec_format(
        "./models/w2vmodel_good.bin",
        binary=True,
        encoding="utf-8",
        unicode_errors="ignore",
    )
    for word in model.vocab:
        print(word, model.vocab[word].index)
    print(len(model.vocab))


def train_fasttext_model(filename):
    sentences = MySentences(train_type="fasttext")
    model_gensim = FastText()
    # build the vocabulary
    model_gensim.build_vocab(sentences)

    # train the model
    model_gensim.train(
        sentences, total_examples=model_gensim.corpus_count, epochs=model_gensim.iter
    )
    print(model_gensim)
    model_gensim.save(filename)


def train_fasttext_wrapper():
    sentences = MySentences(train_type="fasttext")
    with open("./models/text_for_fasttext.txt", "w", encoding="utf-8") as out_text:
        for sentence in sentences:
            out_text.write(" ".join(sentence) + "\n")
    # Set FastText home to the path to the FastText executable
    ft_home = "/home/alena/Programs/FastText/fastText-0.1.0/fasttext"

    # train the model
    model_wrapper = FT_wrapper.train(ft_home, "./models/text_for_fasttext.txt")

    print(model_wrapper)
    # saving a model trained via fastText wrapper
    model_wrapper.save("./models/saved_model_wrapper")


def check_fasttext_model(filename):
    model = FastText.load(filename)
    for word in model.wv.vocab:
        print(word, model.wv.vocab[word].index)
    print(model.wv.vocab["карамбола"].index)
    print(len(model.wv.vocab))


def check_fastetxt_wrapper():
    loaded_model = FT_wrapper.load("./models/saved_model_wrapper")
    print(loaded_model)
    for word in loaded_model.wv.vocab:
        print(word, loaded_model.wv.vocab[word].index)
    print(loaded_model.wv.vocab["келеш"].index)


if __name__ == "__main__":

    train_fasttext_wrapper()
    check_fastetxt_wrapper()

    # train_fasttext_model("./models/saved_fasttext_onlytext.bin")
    # check_fasttext_model("./models/saved_fasttext_onlytext.bin")

    # train_word2vec_model()
    # check_w2v_model()
